#include <assert.h>
#include "bit_map.h"
#include "buddy_allocator.h"
#include <stdio.h>

// returns the number of bytes to store bits booleans
int BitMap_getBytes(int bits){
  return (bits/8) + ((bits%8)!=0);
}

// initializes a bitmap on an external array
void BitMap_init(BitMap* bit_map, int num_bits, uint8_t* buffer){
  bit_map->buffer=buffer;
  bit_map->num_bits=num_bits;
  bit_map->buffer_size=BitMap_getBytes(num_bits);
  assert(bit_map->buffer && bit_map->num_bits && bit_map->buffer_size);
}

// sets a the bit bit_num in the bitmap
// status= 0 or 1
void BitMap_setBit(BitMap* bit_map, int bit_num, int status){
  bit_num = bit_num - 1;
  // get byte
  int byte_num=bit_num>>3;
  assert(byte_num<bit_map->buffer_size);
  //int bit_in_byte=byte_num&0x03;
  int bit_in_byte=bit_num % 8;
  if (status) {
    bit_map->buffer[byte_num] |= (1<<bit_in_byte);
  } else {
    bit_map->buffer[byte_num] &= ~(1<<bit_in_byte);
  }
}

// inspects the status of the bit bit_num
int BitMap_bit(const BitMap* bit_map, int bit_num){
  bit_num = bit_num - 1;
  int byte_num=bit_num>>3; 
  //printf("BYTE NUM: %d\n", byte_num );
  assert(byte_num<bit_map->buffer_size);
  //int bit_in_byte=byte_num&0x03;
  int bit_in_byte = bit_num % 8;
  //printf("Bit IN BYTE: %d\n", bit_in_byte);
  return (bit_map->buffer[byte_num] & (1<<bit_in_byte))!=0;
}

//prints bitmap
void BitMap_print(const BitMap* bit_map){

  printf("===================================================================================\n");

  printf("\t\t\tSTAMPO IL CONTENUTO DELLA BITMAP\n\n");

  /*for(int i=1;i<bit_map->num_bits;i++){
    printf("%d  ", BitMap_bit(bit_map,i));
  }*/
  int levels = levelIdx(bit_map->num_bits);
  int num_spaces = 1 << (levels-1);



  for (int current_level = 0; current_level < levels; current_level++){

    printf("\nLevel %d:\t", current_level);

    //prints spaces
    for(int i = 0; i < num_spaces; i++) printf(" ");

    for (int j = (1<<current_level); j < (1<<(current_level + 1)); j++) printf("%d ", BitMap_bit(bit_map, j));

    num_spaces = num_spaces - ((1<<(current_level + 1))/2);
  }

  printf("\n\n===================================================================================\n\n");
  

}
