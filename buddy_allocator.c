#include <stdio.h>
#include <assert.h>
#include <math.h> // for floor and log2
#include "buddy_allocator.h"

#define BITS_PER_BYTE 8

// these are trivial helpers to support you in case you want
// to do a bitmap implementation
int levelIdx(int idx){
  return (int)floor(log2(idx));
};

int buddyIdx(int idx){
  if (idx&0x1){
    return idx-1;
  }
  return idx+1;
}

int parentIdx(int idx){
  return idx/2;
}

int offset(int idx){
  return (idx-(1<<levelIdx(idx)));
}

int leftChldIdx(int idx){
  return idx*2;
}

int rightChldIdx(int idx){
  return (idx*2) + 1;
}

// computes the size in bytes for the allocator (Restituisce la dimensione della memoria di servizio)
int BuddyAllocator_calcSize(int num_levels) {
  int num_bits = 1<<(num_levels);

  //printf("Il numero di livelli è %d, servono quindi %d bit, che sono %d bytes\n", num_levels,num_bits, (num_bits / BITS_PER_BYTE) + ((num_bits%8)!=0));

  return (num_bits / BITS_PER_BYTE) + ((num_bits%8)!=0);
}


void BuddyAllocator_init(BuddyAllocator* alloc,
                         uint8_t* buffer,  //buffer for bitmap
                         int buffer_size, //number of elements of buffer
                         BitMap* bit_map,
                         int num_levels,
                         char* memory,
                         int min_bucket_size){

  // we need room also for level 0
  alloc->num_levels=num_levels;
  alloc->memory=memory;
  alloc->min_bucket_size=min_bucket_size;
  assert (num_levels<MAX_LEVELS);

  // we need enough memory to handle internal structures
  assert (buffer_size>=BuddyAllocator_calcSize(num_levels));

  printf("\tLevels: %d\n", num_levels);
  printf("\tNumber of bit for bitmap: %d, that are %d bytes\n", buffer_size*BITS_PER_BYTE, buffer_size );
  printf("\tMin bucket size:%d bytes\n", min_bucket_size);
  printf("\tManaged memory: %d bytes\n", (1<<(num_levels-1))*min_bucket_size);
  printf("\tFirst address of memory: %p\n\tLast memory address: %p\n\n", alloc->memory, alloc->memory + (1<<(num_levels-1))*min_bucket_size);
  
  BitMap_init(bit_map,buffer_size*BITS_PER_BYTE,buffer);

  alloc->bit_map = *bit_map;


  for(int i=0;i<bit_map->buffer_size;i++){
    bit_map->buffer[i] = 0;
  }


  printf("\tBITMAP INITIALIZED\n");

  
}

//BITMAP->NUM_BITS CONTIENE BUFFER_SIZE*BITS_PER_BYTE, QUINDI CONTIENE UN BIT IN PIÙ DEI BIT CHE MI SERVONO


//FUNZIONI PER IL CONTROLLO 
//CONTROLLA RICORSIVAMENTE SE IL GENITORE E' ALLOCATO
int BuddyAllocator_controlParent(BitMap* bit_map,int idx){
  if(idx<1){ 
    //printf("FINE CONTROLLO GENITORE-----------------------------------");
    return 1;
  }
  else if(idx==1){
    //printf("IDX = 1 ------------------------------");
    return !BitMap_bit(bit_map,idx);
  }
  //printf("Sto ispezionando l'indice %d\n", idx);  
  return !BitMap_bit(bit_map,idx) && BuddyAllocator_controlParent(bit_map,parentIdx(idx));
}

//CONTROLLA RICORSIVAMENTE SE I FIGLI SONO ALLOCATI
int BuddyAllocator_controlChld(BitMap* bit_map,int idx){
  if(idx>bit_map->num_bits-1){
    //printf("FINE CONTROLLO FIGLI---------------------------------------");
    return 1;
  }
  //printf("Sto ispezionando l'indice %d\n",idx);
  int left = BuddyAllocator_controlChld(bit_map,leftChldIdx(idx));
  int right = BuddyAllocator_controlChld(bit_map,rightChldIdx(idx));
  return !BitMap_bit(bit_map,idx) && left && right;

 // return !BitMap_bit(bit_map,idx-1) && BuddyAllocator_controlChld(bit_map,leftChldIdx(idx)) && BuddyAllocator_controlChld(bit_map,rightChldIdx(idx));
}

//RESTITUISCE L'INDICE SULLA BITMAP DELL'AREA DI MEMORIA. IL PRIMO INDICE È 0.
//SE NON E' DISPONIBILE MEMORIA RESTITUISCE -1
int BuddyAllocator_getBuddy(BuddyAllocator* alloc, int level){
  if (level<0)
    return -1;
  assert(level < alloc->num_levels);

  //TRAMITE IL LIVELLO CONTROLLO SE CI SONO AREE DI MEMORIA A QUEL LIVELLO DISPONIBILI.
  //SE NE TROVO ALMENO UNA DEVO POI CONTROLLARE CHE SIA ASSEGNABILE E QUINDI CHE NON SIANO
  //GIÀ ASSEGNATI I FIGLI O IL GENITORE
  int first_idx = 1<<level;

  int last_idx = 1<<(level+1);


  //printf("HO RICHIESTO MEMORIA AL LIVELLO %d, INDICE INIZIALE %d, INDICE FINALE %d, MASSIMO INDICE ALBERO %d\n", level,first_idx+1,last_idx,alloc->bit_map.num_bits-1);

  for(int i = first_idx; i < last_idx; i++){

    //printf("--------------Indice %d --------------------\n",i);

    if( !BitMap_bit(&alloc->bit_map,i) && BuddyAllocator_controlParent(&alloc->bit_map,parentIdx(i)) &&  BuddyAllocator_controlChld(&alloc->bit_map,leftChldIdx(i)) &&  BuddyAllocator_controlChld(&alloc->bit_map,rightChldIdx(i)) ){
      BitMap_setBit(&alloc->bit_map,i,1);
      return i;
    }

    /*int v = BitMap_bit(&alloc->bit_map,i);
    printf("Controllo bit: %d\n", v);

    int p = BuddyAllocator_controlParent(&alloc->bit_map,parentIdx(i+1));
    printf("Controllo Genitori: %d\n", p);

    int c = BuddyAllocator_controlChld(&alloc->bit_map,i+1);
    printf("Controllo figli: %d\n", c);*/
  }

  return -1;
}

void BuddyAllocator_releaseBuddy(BuddyAllocator* alloc, int index){
  BitMap_setBit(&alloc->bit_map,index,0);
}

//allocates memory
void* BuddyAllocator_malloc(BuddyAllocator* alloc, int size) {
  // we determine the level of the page
  int mem_size=(1<<(alloc->num_levels-1))*alloc->min_bucket_size;
  int  level=floor(log2(mem_size/(size+sizeof(int))));

  // if the level is too small, we pad it to max
  if (level>alloc->num_levels-1)
    level=alloc->num_levels-1;

  //printf("requested: %d bytes, level %d \n",size, level);

  // we get a buddy of that size;
  int buddy_index = BuddyAllocator_getBuddy(alloc, level);
  if (buddy_index < 1){
    printf("******************************MALLOC******************************\n\n");
    printf("\tRequested %d bytes\n\n  It's not possible to assign memory, too little space available\n\n", size);
    printf("******************************************************************\n\n");
    return NULL;
  }
  //we know level and buddy_index
  int* starting_memory =(int *) alloc->memory;

  int off = offset(buddy_index);

  //printf("Index of buddy %d, offset %d\n",buddy_index,off);

  //salva la dimensione di ogni blocco a questo livello
  int memory_slot_size = mem_size / (1<<level);

  int* ris = starting_memory + (off * memory_slot_size)/sizeof(int);

  //printf("\n\nLA MEMORIA INIZIA ALL'INDIRIZZO %p,MEM size %d, HO UN OFFSET DI %d POSIZIONI, UN MEMORY SLOT SIZE DI %d, RIS E' %p\n\n",alloc->memory,mem_size,off,memory_slot_size,ris);

  // we write in the memory region managed the buddy address
  *ris = buddy_index;
  //printf("L'indice dell'area di memoria asseganta è: %d\n\t L'indirizzo è: %p, Inizio memoria utilizzabile dall'utente: %p\n", *ris,ris, ris + 1);

  printf("******************************MALLOC******************************\n\n");
  printf("\tRequested %d bytes, level: %d\n\n\tIndex of buddy assigned: %d\n\n\tStarting address of memory region: %p\n\n\tBeginning of user memory: %p\n\n",size,level,*ris,ris,ris + 1);
  printf("******************************************************************\n\n");
  return ris + 1;
}
//releases allocated memory
void BuddyAllocator_free(BuddyAllocator* alloc, void* mem) {

  //we have to controll if is allocated
  if( mem != NULL ){

    // we retrieve the buddy from the system
    int* p =(int*) mem;
    int index_to_free =*(p-1);
    
    //we have to control that memory is aligned
    int bucket_dim = alloc->min_bucket_size * (1 << ((alloc->num_levels)-1 - levelIdx(index_to_free)));
    char *correct_address = alloc->memory + offset(index_to_free) * bucket_dim;
    if((int *)correct_address != p - 1){
      printf("-------------------------FREE-------------------------\n\n");
      printf("\tPointer not alligned\n\n");
      printf("------------------------------------------------------\n\n");
      return;
    }

    //we have to control that is an allocated memory, if not double free
    if(!BitMap_bit(&alloc->bit_map,index_to_free)){
      printf("-------------------------FREE-------------------------\n\n");
      printf("\tDouble free\n\n");
      printf("------------------------------------------------------\n\n");
      return;
    }

    //we controll that index is correct, trivial
    if(index_to_free < 1 || index_to_free > (1<<(alloc->num_levels + 1)) - 1){
      printf("-------------------------FREE-------------------------\n\n");
      printf("ERROR, you are trying to free a buddy of an index that is not in tree\n\n");
      printf("------------------------------------------------------\n\n");
      return;
    }


    printf("-------------------------FREE-------------------------\n\n");
    printf("\tfreeing %p, Index %d\n\n", mem, *(p-1));
    printf("------------------------------------------------------\n\n");
    BuddyAllocator_releaseBuddy(alloc,index_to_free);
  }
  else{
    printf("-------------------------FREE-------------------------\n\n");
    printf("\tYou are trying to free an unassigned memory\n\n");
    printf("------------------------------------------------------\n\n");
  }
  


  
}
