#include "buddy_allocator.h"
#include <stdio.h>

//#define BUFFER_SIZE 102400
#define BUDDY_LEVELS 6       //DEFAULT 9
#define MEMORY_SIZE  1024        //DEFAULT (1024*1024)
#define MIN_BUCKET_SIZE (MEMORY_SIZE>>(BUDDY_LEVELS-1))

char memory[MEMORY_SIZE];

BuddyAllocator alloc;
BitMap bit_map;
int main(int argc, char** argv) {

  //1 we see if we have enough memory for the buffer
  int req_size=BuddyAllocator_calcSize(BUDDY_LEVELS);
  //printf("size of buffer of bitmap: %d Bytes\n", req_size);

  uint8_t buffer[req_size];

  //2 we initialize the allocator
  printf("INITIALIZING OF ALLOCATOR.......\n\n");
  BuddyAllocator_init(&alloc,
                      buffer,
                      req_size,
                      &bit_map,
                      BUDDY_LEVELS,
                      memory,
                      MIN_BUCKET_SIZE);
  printf("\nDONE\n");

  BitMap_print(&bit_map);

  int* p1 = (int *)BuddyAllocator_malloc(&alloc, 20*sizeof(int));

  for(int i=0;i<20;i++){
    p1[i] = i;
  }

  //stampo p1
  printf("========================Stampo l'array di interi in p1========================\n\n    [ ");
  for(int i=0;i<20;i++){
    if(i==19) printf("%d ",p1[i]);
    else printf("%d, ",p1[i]);
  }
  printf("]\n\n==============================================================================\n\n");

  BitMap_print(&bit_map);


  char* p2= (char*) BuddyAllocator_malloc(&alloc, 15*sizeof(char));

  for(int i=0;i<15;i++){
    if(i%2==0) p2[i] = 'a';
    else p2[i] = 'B';
  }

  //stampo p2
  printf("========================Stampo la stringa p2========================\n\n    [ ");
  for(int i=0;i<15;i++){
    if(i==14) printf("%c ",p2[i]);
    else printf("%c, ",p2[i]);
  }
  printf("]\n\n==============================================================================\n\n");

  BitMap_print(&bit_map);


  void* p3=BuddyAllocator_malloc(&alloc, 100000);


  float* p4=(float *)BuddyAllocator_malloc(&alloc, 10*sizeof(float));
  int* p5=BuddyAllocator_malloc(&alloc, 100*sizeof(int));

  p4[0] = 5.3;
  p4[1] = 55.7866;

  p5[0] = 100;
  p5[99] = 99;

  BitMap_print(&bit_map);

  BuddyAllocator_free(&alloc, (void *)p1);
  BuddyAllocator_free(&alloc, (void *)p2);
  BuddyAllocator_free(&alloc, (void *)p4);
  BuddyAllocator_free(&alloc, (void *)p5);

  BitMap_print(&bit_map);


  //ORA ALLOCO UN BLOCCO GRANDE COME TUTTA LA MEMORIA POSSIBILE
  p3 = BuddyAllocator_malloc(&alloc, 1020);
  
  BitMap_print(&bit_map);

  BuddyAllocator_free(&alloc, (void *)p3);

  //FACCO UN ARRAY DI INTERI RICHIEDENDO OGNI VOLTA UN BUDDY, ENORME SPRECO DI MEMORIA
  int* v[32];
  for(int i=0;i<32;i++){

    v[i] =(int *)BuddyAllocator_malloc(&alloc,sizeof(int));

    *v[i] = i; 

  }

  BitMap_print(&bit_map);


  //provo ad allocare un ulteriore spazio di memoria, DEVE FALLIRE
  int* c = (int *)BuddyAllocator_malloc(&alloc,sizeof(int));
  if(c) printf(" "); //MESSO PER TOGLIERE WARNING

  printf("========================Stampo l'array di interi v========================\n\n    [ ");
  for(int i=0;i<32;i++){
    if(i==31) printf("%d ",*v[i]);
    else printf("%d, ",*v[i]);
  }
  printf("]\n\n==============================================================================\n\n");

  for(int i=0;i<32;i++){

    BuddyAllocator_free(&alloc, (void *)v[i]);

  }

  BitMap_print(&bit_map);



  int* p6= (int *)BuddyAllocator_malloc(&alloc, 2*sizeof(int));
  p6[0] = 1;
  p6[1] = 2;

  void* p7=BuddyAllocator_malloc(&alloc, 1000);//DEVE FALLIRE
  if(p7) printf(" ");

  
  BuddyAllocator_free(&alloc, (void *)p6);

  
}
