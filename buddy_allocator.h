#pragma once

#include "bit_map.h"

#define MAX_LEVELS 16

typedef struct  {
  
  BitMap bit_map; 
  int num_levels;
  char* memory; // the memory area to be managed
  int min_bucket_size; // the minimum page of RAM that can be returned

} BuddyAllocator;


// computes the size in bytes for the buffer of the allocator
int BuddyAllocator_calcSize(int num_levels);


// initializes the buddy allocator, and checks that the buffer is large enough
void BuddyAllocator_init(BuddyAllocator* alloc,
                         uint8_t* buffer,  //buffer for bitmap
                         int num_bits, //number of bits for bitmap
                         BitMap* tree,
                         int num_levels,
                         char* memory,
                         int min_bucket_size);

// returns the index of a buddy at a given level.
// side effect on the internal structures
// -1 id no memory available
int BuddyAllocator_getBuddy(BuddyAllocator* alloc, int level);


// releases an allocated buddy, performing the necessary joins
// side effect on the internal structures
void BuddyAllocator_releaseBuddy(BuddyAllocator* alloc, int index);

//allocates memory
void* BuddyAllocator_malloc(BuddyAllocator* alloc, int size);

//releases allocated memory
void BuddyAllocator_free(BuddyAllocator* alloc, void* mem);

int BuddyAllocator_controlParent(BitMap* bit_map,int idx);

int BuddyAllocator_controlChld(BitMap* bit_map,int idx);

int levelIdx(int idx);